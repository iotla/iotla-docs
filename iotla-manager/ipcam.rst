Module: IPCam
===============
Introduction
--------
Provides with the functionality to detect faces via common ip-based cameras.
A specific person can be introduced to the system and thus be identified by name.
The actual face detection functionality has been implemented by the face-api.js (https://github.com/justadudewhohacks/face-api.js/).
Supported is a wide-range of ip-cam protocols:

- ONVIF
- JMPEG
- RTSP

The manager offers a media reference which can be fetched by the web-client in order to display the current ip-cam's detection.
Additionally, the detected persons (specifically by name or as 'Unknown') are added to the collected data and therefore later may be used for the rule-engine to trigger subsequent actions.

Custom Configuration
--------

- **username**: Username that is used for authentication with the camera
- **password**: Password that is used for authentication with the camera
- **rtsp_stream_port_and_path**: Port and the subsequent URL-path for RTSP endpoint of the camera (does not include IP!), e.g. "**:554/video.sdp**"
- **mjpeg_stream_port_and_path**: Port and the subsequent URL-path for MJPEG endpoint of the camera (does not include IP!), e.g. "**:80/vb.htm**"
