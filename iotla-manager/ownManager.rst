Create your own module
===============
Introduction
--------
One of the main ideas of the IOTLA project is it to allow integrate new device types with basic knowledge of
JavaScript programming.

The following explanations will give you the toolkit to create your own manager module.

Module components
--------
A module is a folder with the following components:

- **package.json**: There you define additional software packages you need, the name and the version of your module
- **<FOLDERNAME>Config.js**: Holds basic configuration for the sensor and the actor component such us defining sensor value and actor actions.
- **<FOLDERNAME>Sensor.js**: Fetches the sensor data from an IoT device, Web Applications or other sources. The data will then send to the IOTLA system.
- **<FOLDERNAME>Actor.js**: Allows to define actions that are possible to execute by the IOTLA system

That was a basic introduction to the four existing components. Now every component will be explained by a example
that you can use to define your own module.

**IMPORTANT:** You have to replace <FOLDERNAME> with your module name im camel case format:

Example: GoogleCalendar, OpenWeatherMap etc.

Component: package.json
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
TBD

.. code-block:: JSON

    {
      "name": "iotla-module-<FOLDERNAME LOWERCASE -> Example: GoogleCalendar => google-calendar>",
      "version": "1.0.0",
      "description": "Here a description of the module",
      "dependencies": {
        "node-fetch": "2.6.0"
      },
      "keywords": [
        "IoT",
        "HTTP",
        "Sensor"
      ],
      "author": "Firstname Name",
      "license": "Apache"
    }


Component: <FOLDERNAME>Config.js
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
TBD

.. code-block:: javascript

    module.exports = {
        category: "MultiSensor",
        hasDataSources: true,
        type: "job",
        cronTabEntry: "* * * * *",
        customConfig: {
            displayName: {
                type: "string",
                value: "Example",
                defaultValue: "Example",
                validation: {
                    required: true
                }
            }
        },
        i18n: {
            de: {
                displayName: "Name"
            },
            en: {
                displayName: "Name"
            }
        },
        actions: {
            powerOn: {
                handle: "handle<ACTION_NAME>",
                input: null
            }
        },
        sensors: [
            {key: "currentPressure",         type: "float",      historySize: 5},
            {key: "currentGas",              type: "float",      historySize: 5},
            {key: "currentAltitude",         type: "float",      historySize: 5},
            {key: "currentTemperature",      type: "float",      historySize: 5},
            {key: "currentRelativeHumidity", type: "float",      historySize: 5},
            {key: "airQuality",              type: "integer",    historySize: 5},
        ]
    };

Component: <FOLDERNAME>Sensor.js
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
TBD

.. code-block:: javascript

    const SensorHandler = require('../../SensorHandler.js');

    const fetch         = require('node-fetch');

    class <FOLDERNAME>Sensor extends SensorHandler {
        constructor(manager, config){ super(manager, config) }

        async fetchData(){
            this.forEachDataSource(async ds => {
                try {
                    const response = await fetch(`http://${ds.ipAddress}`);
                    if (response.ok){
                        const data = await response.json();
                        this.forEachSensor(s => this.updateSensorValue(s, { value: data[s.key] }));
                    }
                }catch (err) {
                    this.log("debug", err);
                }
            });
        }
    }

    module.exports = <FOLDERNAME>Sensor;

Component: <FOLDERNAME>Actor.js
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    const ActorHandler       = require('../../ActorHandler.js');

    class <FOLDERNAME>Actor extends ActorHandler {
        constructor(manager, config) { super(manager, config) }

        async _handle<ACTION_NAME>(that, action){
            const ds = await that.getTargetactionSource(action.target);
            if (ds === null) return;
            try {
                [...]
                    Your code
                [...]
                that.successResponseHandle(action);
            }catch (err) {
                that.errorResponseHandle(action, err.toString());
            }
        }
    }

    module.exports = <FOLDERNAME>Actor;

