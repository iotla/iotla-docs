
IOTLA Quick Start
===============

Raspberry PI SD card image
------------------------------

The IOTLA system is available as Raspbian image.
The following steps will show you how to setup the system on your raspberry pi.

Requisition
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Raspberry Pi 3
* SD Card
* Smart phone / Tablet / PC

1. Download the latest RaspIOTLA image:
2. Use Etcher to flash the image on the sd card: `Etcher <https://www.balena.io/etcher/>`_
3. Connect the Raspberry Pi with the power supply
4. Go to your WiFi overview and waiting for the WLAN with the name: IOTLA-Setup
5. Select the IOTLA-Setup WLAN

.. figure:: ../img/setup/IPhoneScreen4.png
    :name: IPhone screen 4
    :alt:  IPhone screen 4
    :align: center
    :width: 20%

    WLAN IOTLA-Setup show up and is selected








